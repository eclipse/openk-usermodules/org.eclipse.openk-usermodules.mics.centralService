/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;

public class MicsCentralConfiguration extends Configuration {
    @NotEmpty
    private String servicesDistributionFileName;

    @JsonProperty
    private String proxyHost;

    @JsonProperty
    private String proxyPort;

    @JsonProperty
    public String getServicesDistributionFileName() {
        return servicesDistributionFileName;
    }

    @JsonProperty
    public void setServicesDistributionFileName(String servicesDistributionFileName) {
        this.servicesDistributionFileName = servicesDistributionFileName;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }
}
