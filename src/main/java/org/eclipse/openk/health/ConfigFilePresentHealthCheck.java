/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.health;

import com.codahale.metrics.health.HealthCheck;
import org.eclipse.openk.api.ServiceDistributionCluster;
import org.eclipse.openk.core.controller.ServicesConfigCache;

public class ConfigFilePresentHealthCheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {
        ServiceDistributionCluster[] sdc = ServicesConfigCache.getInstance().getCache(); // Throws Exception if it fails

        if (sdc != null && sdc.length > 0) {
            return Result.healthy();
        }
        else {
            return Result.unhealthy("No ServiceDistributionCluster available!");
        }
    }
}
