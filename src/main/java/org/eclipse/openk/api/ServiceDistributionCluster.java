/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;


public class ServiceDistributionCluster {

    public static class ServiceDistribution {

        private boolean active;

        @NotEmpty
        private String name;

        @NotEmpty
        private String host;

        @NotEmpty
        private String urlPath;

        private String healthUrlPath;

        @NotEmpty
        private String protocol;

        @NotEmpty
        private Integer portApp;

        private Integer portHealth;

        private String description;

        public ServiceDistribution() {
            // public default construction
        }

        @JsonProperty
        public boolean isActive() {
            return active;
        }

        @JsonProperty
        public void setActive(boolean active) {
            this.active = active;
        }

        @JsonProperty
        public String getName() {
            return name;
        }

        @JsonProperty
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty
        public String getHost() {
            return host;
        }

        @JsonProperty
        public void setHost(String host) {
            this.host = host;
        }

        @JsonProperty
        public String getUrlPath() {
            return urlPath;
        }

        @JsonProperty
        public void setUrlPath(String urlPath) {
            this.urlPath = urlPath;
        }

        public String getHealthUrlPath() {
            return healthUrlPath;
        }

        public void setHealthUrlPath(String healthUrlPath) {
            this.healthUrlPath = healthUrlPath;
        }

        @JsonProperty
        public String getProtocol() {
            return protocol;
        }

        @JsonProperty
        public void setProtocol(String protocol) {
            this.protocol = protocol;
        }

        @JsonProperty
        public int getPortApp() {
            return portApp;
        }

        @JsonProperty
        public void setPortApp(int portApp) {
            this.portApp = portApp;
        }

        @JsonProperty
        public String getDescription() {
            return description;
        }

        @JsonProperty
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty
        public Integer getPortHealth() {
            return portHealth;
        }

        @JsonProperty
        public void setPortHealth(Integer portHealth) {
            this.portHealth = portHealth;
        }
    }

    private boolean active;

    @NotEmpty
    private String clustername;

    private String description;

    private ServiceDistribution[] distributions;

    @JsonProperty
    public boolean isActive() {
        return active;
    }

    @JsonProperty
    public void setActive(boolean active) {
        this.active = active;
    }

    @JsonProperty
    public String getClustername() {
        return clustername;
    }

    @JsonProperty
    public void setClustername(String clustername) {
        this.clustername = clustername;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    @JsonProperty
    public void setDescription(String description) {
        this.description = description;
    }

    public ServiceDistribution[] getDistributions() {
        return distributions;
    }

    public void setDistributions(ServiceDistribution[] distributions) {
        this.distributions = distributions;
    }
}
