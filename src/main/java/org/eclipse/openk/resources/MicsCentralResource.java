/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.resources;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.log4j.Logger;
import org.eclipse.openk.api.VersionInfo;
import org.eclipse.openk.core.common.JsonGeneratorBase;
import org.eclipse.openk.core.controller.DispatchController;
import org.hibernate.validator.constraints.NotEmpty;
import org.eclipse.openk.api.ServiceRequestEnvelope;
import org.eclipse.openk.core.controller.CentralController;
import org.eclipse.openk.core.controller.BaseWebService;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(value = "/mics/gridmeasures")
@ApiResponses( value ={
        @ApiResponse(code = 200, message = "OK",response = VersionInfo.class,reference = "#/definitions/VersionInfo"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 404, message = "Not found"),
        @ApiResponse(code = 423, message = "Locked"),
        @ApiResponse(code = 500, message = "Internal Server Error") } )
@Path("/mics/central")
@Produces(MediaType.APPLICATION_JSON)
public class MicsCentralResource extends BaseWebService{
    private static Logger logger = Logger.getLogger(MicsCentralResource.class.getName());

    public MicsCentralResource() {
        super(logger);
    }

    @ApiOperation(value = "Service Distribution", notes = "This service returns the available distributions for a given cluster")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK") } )
    @GET
    @Path("/serviceDistribution/{clustername}")
    public Response getServiceDistribution(@ApiParam(name = "clustername", value = "The name of the cluster")
                                           @PathParam("clustername") @NotEmpty String clustername) {
        return invokeRunnable(() -> new CentralController().readServerDistribution(clustername));
    }

    @ApiOperation(value = "Health state check", notes = "This service checks if the components of a service are healthy")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK") } )
    @GET
    @Path("/serviceHealthState/{clustername}/{servicename}")
    public Response getClusterHealthState(@ApiParam(name = "clustername", value = "The name of the cluster")
                                          @PathParam("clustername") @NotEmpty String clustername,
                                          @ApiParam(name = "servicename", value = "The name of the service")
                                          @PathParam("servicename") @NotEmpty String servicename) {
        return invokeRunnable(() -> new CentralController().getServiceHealthState(clustername, servicename));
    }

    @ApiOperation(value = "Dispatcher", notes = "")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK") } )
    @POST
    @Path("/dispatch/{clustername}")
    public Response dispatch(String envelope,
                             @ApiParam(name = "clustername", value = "The name of the cluster")
                             @PathParam("clustername") @NotEmpty String clustername) {
        ServiceRequestEnvelope envObj = JsonGeneratorBase.getGson().fromJson(envelope, ServiceRequestEnvelope.class);

        try {
            return new DispatchController().dispatch(clustername, envObj);
        } catch (HttpStatusException hse) {
            logger.error(hse);
            return Response.status(hse.getHttpStatus()).build();
        }
    }

    @ApiOperation(value = "Version Information", notes = "This services displays the version infos of this backend and the connected database.")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = VersionInfo.class,reference = "#/definitions/VersionInfo") } )
    @GET
    @Path("/versionInfo")
    public Response getVersionInfo() {
        return invokeRunnable(() -> new CentralController().getVersionInfo(getVersionString()));
    }
}
