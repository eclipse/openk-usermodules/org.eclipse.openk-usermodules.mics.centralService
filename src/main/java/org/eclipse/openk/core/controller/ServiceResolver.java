/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.eclipse.openk.api.ServiceDistributionCluster;
import org.eclipse.openk.api.ServiceDistributionCluster.ServiceDistribution;

import java.util.Arrays;
import java.util.Optional;

public class ServiceResolver {
    private final ServiceDistributionCluster[] clusters;

    public ServiceResolver(ServiceDistributionCluster[] clusters) {
        this.clusters = clusters;
    }

    public ServiceDistribution resolve(String clustername, String servicename) {
        Optional<ServiceDistributionCluster> cluster
                = Arrays.stream(clusters).filter(
                x -> x.getClustername().equalsIgnoreCase(clustername)).findFirst();
        if (cluster.isPresent()) {
            Optional<ServiceDistributionCluster.ServiceDistribution> dist =
                    Arrays.stream(cluster.get().getDistributions())
                            .filter(x -> x.getName().equalsIgnoreCase(servicename))
                            .findFirst();
            if (dist.isPresent()) {
                return dist.get();
            }
        }
        return null;
    }
}
