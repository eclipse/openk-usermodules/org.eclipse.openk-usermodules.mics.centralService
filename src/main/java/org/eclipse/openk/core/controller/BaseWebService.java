/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Properties;

public class BaseWebService {
    @FunctionalInterface
    public interface Invokable <T> {
        public T invoke() throws Exception; // NOSONAR
    }
    private final Logger logger;

    protected BaseWebService(Logger logger) {
        this.logger = logger;
    }

    protected Response invokeRunnable(Invokable runnable)
    {
        try  { // NOSONAR
            Object o =  runnable.invoke();
            return Response.ok(o).build();
        } catch (HttpStatusException hse) {
            logger.error(hse);
            return Response.status(hse.getHttpStatus()).build();
        }
        catch (Exception e) {
            logger.error("Caught unexpected Exception:", e);
            return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).build();
        }
    }


    protected String getVersionString() {
        try {
            // determine static VersionInfo
            final Properties properties = new Properties();

            properties.load(BaseWebService.class.getClassLoader().getResourceAsStream("project.properties"));

            String beversion = properties.getProperty("backend.version");
            if( beversion.contains("$")) {
                beversion = "LOCAL-DEV";
            }
            return beversion;
        } catch (IOException e) {
            logger.error("Exception reading the properties file", e);
            throw new RuntimeException("Exception during start up"); // NOSONAR
        }
    }



}
