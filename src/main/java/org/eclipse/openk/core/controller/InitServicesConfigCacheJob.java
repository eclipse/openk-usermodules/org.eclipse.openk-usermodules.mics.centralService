/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.apache.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

public class InitServicesConfigCacheJob {

    private static final Logger LOGGER = Logger.getLogger(InitServicesConfigCacheJob.class.getName());

    private InitServicesConfigCacheJob() {
    }

    public static void init( String configFileName ) {
        LOGGER.info("InitServicesConfigCacheJob called");
        TimerTask timerTask = new ServicesConfigCacheTimerTask(configFileName);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 100, 5000L);
    }
}
