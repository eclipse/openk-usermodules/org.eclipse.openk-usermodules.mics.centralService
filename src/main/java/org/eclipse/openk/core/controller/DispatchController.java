/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.api.ServiceDistributionCluster;
import org.eclipse.openk.api.ServiceDistributionCluster.ServiceDistribution;
import org.eclipse.openk.api.ServiceRequestEnvelope;
import org.eclipse.openk.core.communication.RestServiceWrapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import javax.ws.rs.core.Response;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class DispatchController {
    private static Logger logger = Logger.getLogger(DispatchController.class);

    public Response dispatch(String clustername, ServiceRequestEnvelope envelope) throws HttpStatusException {

        List<RestServiceWrapper.HttpHeader> transformedList = Lists.newArrayList(envelope.getHeaders()).stream()
                .map(header ->
                        RestServiceWrapper.createHeader(header.getAttribute(), header.getValue())).collect(toList());

        String url = locateBaseUrl(ServicesConfigCache.getInstance().getCache(),
                clustername, envelope.getServiceName())
                + envelope.getUriFragment();

        RestServiceWrapper rsWrap = createRestServiceWrapper(url.toUpperCase().startsWith("HTTPS"));
        return rsWrap.performHttpRequest(resolveMethod(envelope.getMethod()),
                url, transformedList, envelope.getPayloadDecode());
    }

    protected RestServiceWrapper createRestServiceWrapper(boolean useHttps) {
        return new RestServiceWrapper(useHttps);
    }

    private String locateBaseUrl(ServiceDistributionCluster[] cluster, String clustername, String servicename) throws HttpStatusException {
        ServiceDistribution dist = new ServiceResolver(cluster)
                .resolve(clustername, servicename);
        if (dist == null) {
            logger.error("Service [" + clustername + "]/[" + servicename + "] is not resolvable!");
            throw new HttpStatusException(HttpStatus.NOT_FOUND_404);
        }
        return dist.getProtocol() + "://" + dist.getHost() + ":" + dist.getPortApp() + dist.getUrlPath();
    }

    private RestServiceWrapper.HttpMethod resolveMethod(String method) throws HttpStatusException {
        switch (method.toUpperCase()) {
            case "GET":
                return RestServiceWrapper.HttpMethod.GET;
            case "POST":
                return RestServiceWrapper.HttpMethod.POST;
            case "PUT":
                return RestServiceWrapper.HttpMethod.PUT;
            case "DELETE":
                return RestServiceWrapper.HttpMethod.DELETE;
            default:
                logger.error("Invalid Method: " + method);
                throw new HttpStatusException(HttpStatus.METHOD_NOT_ALLOWED_405);
        }

    }
}
