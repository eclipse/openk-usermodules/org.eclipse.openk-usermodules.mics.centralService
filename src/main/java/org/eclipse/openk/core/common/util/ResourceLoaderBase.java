/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/




package org.eclipse.openk.core.common.util;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.io.StringWriter;

public class ResourceLoaderBase {
    private String stream2String(InputStream is) {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(is, writer, "UTF-8");
        } catch (Exception e) { // NOSONAR
            return "";
        }
        return writer.toString();


    }

    public String loadStringFromResource(String filename) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream jsonstream = classLoader.getResourceAsStream(filename);
        return stream2String(jsonstream);
    }
}
