/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk;

import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.InitServicesConfigCacheJob;
import org.eclipse.openk.health.ConfigFilePresentHealthCheck;
import org.eclipse.openk.resources.MicsCentralResource;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class MicsCentralApplication extends Application<MicsCentralConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MicsCentralApplication().run(args);
    }

    @Override
    public String getName() {
        return "micsCentral";
    }

    @Override
    public void initialize(final Bootstrap<MicsCentralConfiguration> bootstrap) {
        // nothing to do here for now
    }

    @Override
    public void run(final MicsCentralConfiguration configuration,
                    final Environment environment) {

        initAppEnvironment( configuration );

        final MicsCentralResource micsCentralResource = new MicsCentralResource();
        final HealthCheck configFilePresentHC = new ConfigFilePresentHealthCheck();

        environment.healthChecks().register("configFilePresent", configFilePresentHC );
        environment.jersey().register(micsCentralResource);

        configureCors(environment);
    }

    private void initAppEnvironment( MicsCentralConfiguration conf ) {
        InitServicesConfigCacheJob.init(conf.getServicesDistributionFileName());
        BackendConfig.getInstance().setProxyHost(conf.getProxyHost());
        BackendConfig.getInstance().setProxyPort(
                Integer.parseInt(null != conf.getProxyPort() ? conf.getProxyPort() : "-1"));
    }

    private void configureCors(Environment environment) {
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.TIMING_ALLOW_ORIGIN_HEADER, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization,X-XSRF-TOKEN");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        // DO NOT pass a preflight request to down-stream auth filters
        // unauthenticated preflight requests should be permitted by spec
        cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, Boolean.FALSE.toString());
    }

}
