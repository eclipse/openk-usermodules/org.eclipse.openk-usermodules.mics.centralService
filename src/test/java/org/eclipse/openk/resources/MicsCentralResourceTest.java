/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.resources;

import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.core.controller.ServicesConfigCache;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

public class MicsCentralResourceTest {
    @Test
    public void testGetVersionInfo() {
        Response r = new MicsCentralResource().getVersionInfo();
        assertNotNull( r );
    }

    @Test
    public void testGetServiceDistribution() throws HttpStatusException {
        ServicesConfigCache.getInstance().readServerDistribution("servicesDistributionDevLocal.json");
        Response r = new MicsCentralResource().getServiceDistribution("elogbook.openk" );

        assertEquals(HttpStatus.OK_200, r.getStatus());
    }
}
