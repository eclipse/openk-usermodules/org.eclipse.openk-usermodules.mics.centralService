/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.exceptions;


import org.eclipse.jetty.http.HttpStatus;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HttpStatusExceptionTest {
    @Test
    public void testAll() {
        HttpStatusException hse = new HttpStatusException( 200 );
        assertEquals(HttpStatus.OK_200, hse.getHttpStatus());
    }
}
