/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.common;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Calendar;

public class GsonUTCDateAdapterTest {

    @Test
    public void serializeTest() {
        GsonUTCDateAdapter gsonUTCDateAdapter = new GsonUTCDateAdapter();
        Calendar calendar = Calendar.getInstance();
        java.util.Date date = calendar.getTime();
        Type type = new Type() {
            @Override
            public String getTypeName() {
                return null;
            }
        };
        JsonSerializationContext jsonSerializationContext = new JsonSerializationContext() {
            @Override
            public JsonElement serialize(Object o) {
                return null;
            }

            @Override
            public JsonElement serialize(Object o, Type type) {
                return null;
            }
        };

        JsonElement je = gsonUTCDateAdapter.serialize(date, type, jsonSerializationContext);

        // run through generator code base
        JsonGeneratorBase.getGson();


        JsonDeserializationContext jsonDeserializationContext = new JsonDeserializationContext() {
            @Override
            public <T> T deserialize(JsonElement jsonElement, Type type) throws JsonParseException {
                return null;
            }
        };


        gsonUTCDateAdapter.deserialize(je, type, jsonDeserializationContext);
    }

}
