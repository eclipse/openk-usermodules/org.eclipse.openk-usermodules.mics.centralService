/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.core.common.util.ResourceLoaderBase;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

public class BaseWebServiceTest extends ResourceLoaderBase {
	private static final org.apache.log4j.Logger EMPTYLOGGER = org.apache.log4j.Logger
			.getLogger(BaseWebServiceTest.class.getName());

	private BaseWebService createBaseWebService() {
		return new BaseWebService(EMPTYLOGGER);
	}

	private BaseWebService.Invokable createInvokable( Object o, Exception e2Return ) {
		return () -> {
            if( e2Return == null ) {
                return o;
            }
            else {
                throw e2Return;
            }
        };
	}

	@Test
	public void testInvokeRunnableOK() {
		Response r = createBaseWebService().invokeRunnable(createInvokable("Test Ok", null));
		assertEquals(HttpStatus.OK_200, r.getStatus());
	}

	@Test
	public void testInvokeRunnable_HttpStatusException() {
		Response r = createBaseWebService().invokeRunnable(createInvokable(null,
				new HttpStatusException(HttpStatus.BAD_REQUEST_400)));
		assertEquals(HttpStatus.BAD_REQUEST_400, r.getStatus());
	}

	@Test
	public void testInvokeRunnable_GeneralException() {
		Response r = createBaseWebService().invokeRunnable(createInvokable( null,
				new Exception("hallodri")));
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR_500, r.getStatus());
	}

	@Test
	public void testGetVersionString() throws Exception {
		BaseWebService bws = createBaseWebService();
		String version = (String) Whitebox.invokeMethod(bws, "getVersionString");
		assertNotNull(version);
		assertNotEquals(version, "");
	}




}
