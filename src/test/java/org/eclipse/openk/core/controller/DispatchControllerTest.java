/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.easymock.EasyMock;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.eclipse.openk.api.ServiceDistributionCluster;
import org.eclipse.openk.api.ServiceRequestEnvelope;
import org.eclipse.openk.core.common.JsonGeneratorBase;
import org.eclipse.openk.core.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.communication.RestServiceWrapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import javax.ws.rs.core.Response;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.junit.Assert.assertEquals;

public class DispatchControllerTest extends ResourceLoaderBase {

    @Before
    public void init() {
        String clusterString = this.loadStringFromResource("servicesDistributionTest.json");
        ServiceDistributionCluster[] cluster = JsonGeneratorBase.getGson()
                .fromJson(clusterString, ServiceDistributionCluster[].class);
        ServicesConfigCache cache = ServicesConfigCache.getInstance();
        Whitebox.setInternalState(cache, "cache", cluster);


    }

    @Test
    public void testDispatch_ok() throws HttpStatusException {
        Response response = Response.ok().entity("{ret: '111'}").build();
        final RestServiceWrapper rester = EasyMock.createMock(RestServiceWrapper.class);
        EasyMock.expect(rester.performHttpRequest(anyObject(),
                anyString(), anyObject(), anyString())).andReturn(response);
        EasyMock.replay(rester);
        ServiceRequestEnvelope envelope = JsonGeneratorBase.getGson().fromJson(
                loadStringFromResource("serviceRequestEnvelope.json"), ServiceRequestEnvelope.class);


        DispatchController controller = new DispatchController() {
            @Override
            protected RestServiceWrapper createRestServiceWrapper(boolean b) {
                return rester;
            }
        };

        Response resp2 = controller.dispatch("elogbook.openk", envelope);
        assertEquals(HttpStatus.OK_200, resp2.getStatus());

    }

    @Test(expected = HttpStatusException.class)
    public void testDispatch_InvCluster() throws HttpStatusException {

        Response response = Response.ok().entity("{ret: '111'}").build();
        final RestServiceWrapper rester = EasyMock.createMock(RestServiceWrapper.class);
        EasyMock.expect(rester.performHttpRequest(anyObject(),
                anyString(), anyObject(), anyString())).andReturn(response);
        EasyMock.replay(rester);
        ServiceRequestEnvelope envelope = JsonGeneratorBase.getGson().fromJson(
                loadStringFromResource("serviceRequestEnvelope.json"), ServiceRequestEnvelope.class);


        DispatchController controller = new DispatchController() {
            @Override
            protected RestServiceWrapper createRestServiceWrapper(boolean b) {
                return rester;
            }
        };

        try {
            controller.dispatch("invalid", envelope);
        } catch (HttpStatusException hse) {
            assertEquals(HttpStatus.NOT_FOUND_404, hse.getHttpStatus());
            throw hse;
        }
    }

    @Test(expected = HttpStatusException.class)
    public void testResolveMethod() throws Exception {
        DispatchController ctrl = new DispatchController();
        assertEquals(RestServiceWrapper.HttpMethod.GET, Whitebox.invokeMethod(ctrl, "resolveMethod", "get"));
        assertEquals(RestServiceWrapper.HttpMethod.POST, Whitebox.invokeMethod(ctrl, "resolveMethod", "Post"));
        assertEquals(RestServiceWrapper.HttpMethod.DELETE, Whitebox.invokeMethod(ctrl, "resolveMethod", "DELETE"));
        assertEquals(RestServiceWrapper.HttpMethod.PUT, Whitebox.invokeMethod(ctrl, "resolveMethod", "put"));

        String s;
        try {
            s = Whitebox.invokeMethod(ctrl, "resolveMethod", "invalid");
        } catch (HttpStatusException hse) {
            // the correct test path goes in here!!!!!!
            assertEquals(HttpStatus.METHOD_NOT_ALLOWED_405, hse.getHttpStatus());
            throw hse;
        }
    }

}
