/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.easymock.EasyMock;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.api.VersionInfo;
import org.eclipse.openk.core.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.communication.RestServiceWrapper;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.eclipse.openk.api.ServiceDistributionCluster;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class CentralControllerTest {

    private ServiceDistributionCluster baseTestReadServerDistribution(String clustername, String jsonFile) throws Exception {
        ResourceLoaderBase resourceLoaderBase = new ResourceLoaderBase();
        String json = resourceLoaderBase.loadStringFromResource(jsonFile);

        ServicesConfigCache scc = ServicesConfigCache.getInstance();
        ServiceDistributionCluster[] sdc = (ServiceDistributionCluster[])
                Whitebox.invokeMethod(scc,"readServerDistributionFromText", json);

        Whitebox.setInternalState(scc, "cache", sdc);

        CentralController be = new CentralController();

        ServiceDistributionCluster ret = be.readServerDistribution(clustername);

        return ret;
    }

    @Test
    public void testReadServerDistribution() throws Exception {

        ServiceDistributionCluster sdc = baseTestReadServerDistribution("elogbook.openK", "testServiceDistributions.json");

        assertEquals("elogbook.openK", sdc.getClustername());
        assertEquals(2, sdc.getDistributions().length);
        assertEquals("172.18.22.160", sdc.getDistributions()[0].getHost());
    }

    @Test
    public void testFailReadServerDistribution() throws Exception {
        try {
            baseTestReadServerDistribution("doesntMatter", "testServiceDist_False.json");
        } catch( HttpStatusException e ) {
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR_500, e.getHttpStatus());
        }
    }

    @Test
    public void testReadServerDistribution_notfound() throws Exception {
        try {
            ServiceDistributionCluster sdc = baseTestReadServerDistribution("invalidCluster", "testServiceDistributions.json");
        } catch( HttpStatusException e ) {
            assertEquals(HttpStatus.NOT_FOUND_404, e.getHttpStatus());
        }
    }

    @Test
    public void testReadServerDistribution_activeInactive() throws Exception {
        ServiceDistributionCluster sdc = baseTestReadServerDistribution( "elogbook.openk", "testServiceDistributionsTwo_OneIsInactive.json");

        assertEquals( 1, sdc.getDistributions().length);
        assertTrue( sdc.getDistributions()[0].isActive());
        assertEquals( "172.18.22.160", sdc.getDistributions()[0].getHost());
    }

    @Test (expected = HttpStatusException.class)
    public void testFindDistribution_notFound() throws Exception {
        ServiceDistributionCluster sdc = baseTestReadServerDistribution("elogbook.openK", "testServiceDistributions.json");
        CentralController cc = new CentralController();
        try {
            Whitebox.invokeMethod(cc, "findDistribution", "elogbook.openK", "testService");
        }
        catch( HttpStatusException hse ) {
            assertEquals(HttpStatus.NOT_FOUND_404, hse.getHttpStatus());
            throw hse;
        }
    }

    @Test
    public void testFindDistribution_ok() throws Exception {
        ServiceDistributionCluster sdc = baseTestReadServerDistribution("elogbook.openK", "testServiceDistributions.json");
        CentralController cc = new CentralController();

        Whitebox.invokeMethod(cc, "findDistribution", "elogbook.openK", "auth-n-auth.mics");

    }


    @Test (expected = HttpStatusException.class)
    public void testFindDistribution_clusterNotFound() throws Exception {
        ServiceDistributionCluster sdc = baseTestReadServerDistribution("elogbook.openK", "testServiceDistributions.json");
        CentralController cc = new CentralController();
        try {
            Whitebox.invokeMethod(cc, "findDistribution", "invalidCluster", "auth-n-auth.mics");
        }
        catch( HttpStatusException hse ) {
            assertEquals(HttpStatus.NOT_FOUND_404, hse.getHttpStatus());
            throw hse;
        }
    }

    @Test
    public void testGetVersionInfo() throws Exception {
        CentralController cc = new CentralController();
        assertEquals("Bruno2.0", cc.getVersionInfo("Bruno2.0").getBackendVersion());
        assertTrue(Whitebox.invokeMethod(cc, "createRestServiceWrapper", true) instanceof RestServiceWrapper );
    }


    @Test
    public void testHealthCheck() throws Exception {
        ServiceDistributionCluster sdc = baseTestReadServerDistribution("elogbook.openK", "testServiceDistributions.json");
        CentralController cc = new CentralController() {
            @Override
            protected RestServiceWrapper createRestServiceWrapper( boolean isHttps ) {
                RestServiceWrapper wrapper = EasyMock.createMock( RestServiceWrapper.class );
                try {
                    expect( wrapper.performGetRequest(  anyObject()) ).andReturn("Ist OK!").anyTimes();
                } catch (HttpStatusException e) {
                    return null;
                }
                replay( wrapper );
                verify( wrapper );
                return wrapper;
            }

        };


        assertEquals("Ist OK!", cc.getServiceHealthState("elogbook.openK", "auth-n-auth.mics"));
        assertEquals("Ist OK!", cc.getServiceHealthState("elogbook.openK", "homie"));

    }
}
