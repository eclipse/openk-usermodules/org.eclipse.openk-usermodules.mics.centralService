/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.easymock.EasyMock;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import static org.easymock.EasyMock.*;

public class ServicesConfigCacheTimerTaskTest {
    private static ServicesConfigCache saveScc;

    @BeforeClass
    public static void SaveState() {
        saveScc = Whitebox.getInternalState(ServicesConfigCache.class, "instance");

    }

    @AfterClass
    public static void tearDown() {
        Whitebox.setInternalState(ServicesConfigCache.class, "instance", saveScc);
    }

    @Test
    public void testRun() throws Exception {
        // ------------------ Prepare Test ----------------------
        final String EXCEPTION_TEXT = "Exception";

        // just to reach the sonar quality gate
        Whitebox.invokeConstructor(InitServicesConfigCacheJob.class);

        ServicesConfigCache scc = EasyMock.createMock(ServicesConfigCache.class);
        scc.readServerDistribution(eq(EXCEPTION_TEXT));
        expectLastCall().andThrow(new HttpStatusException(HttpStatus.INTERNAL_SERVER_ERROR_500)).anyTimes();

        scc.readServerDistribution(not(eq(EXCEPTION_TEXT)));
        expectLastCall().anyTimes();

        replay( scc );

        Whitebox.setInternalState(ServicesConfigCache.class, "instance", scc);


        // ---------------------------- Test it ------------------------------------------
        ServicesConfigCacheTimerTask task = new ServicesConfigCacheTimerTask(EXCEPTION_TEXT);

        task.run();

        // should run without problems
        ServicesConfigCacheTimerTask task2 = new ServicesConfigCacheTimerTask("No Error");
        task2.run();

        tearDown();
    }

}
