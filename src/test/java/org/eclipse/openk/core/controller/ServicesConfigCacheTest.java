/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.eclipse.openk.api.ServiceDistributionCluster;
import org.eclipse.openk.core.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import static org.junit.Assert.assertEquals;


public class ServicesConfigCacheTest {

    private ServiceDistributionCluster[] baseTestReadServerDistribution(String jsonFile) throws Exception {
        ResourceLoaderBase resourceLoaderBase = new ResourceLoaderBase();
        String json = resourceLoaderBase.loadStringFromResource(jsonFile);

        ServicesConfigCache scc = ServicesConfigCache.getInstance();
        return Whitebox.invokeMethod(scc, "readServerDistributionFromText", json);
    }

    @Test
    public void testReadServerDistribution() throws Exception {

        ServiceDistributionCluster[] sdc = baseTestReadServerDistribution("testServiceDistributions.json");

        assertEquals("elogbook.openK", sdc[0].getClustername());
        assertEquals(2, sdc[0].getDistributions().length);
        assertEquals("172.18.22.160", sdc[0].getDistributions()[0].getHost());
    }

    @Test
    public void testFailReadServerDistribution() throws Exception {
        try {
            baseTestReadServerDistribution("testServiceDist_False.json");
        } catch( HttpStatusException e ) {
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR_500, e.getHttpStatus());
        }
    }

    @Test
    public void testReadServerDistribution_FileNotExist() throws Exception {
        try {
            baseTestReadServerDistribution("Nowhere.json");
        } catch (HttpStatusException e) {
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR_500, e.getHttpStatus());
        }
    }

    @Test
    public void testReadServerDistribution_FileNotExistEither() throws Exception {
        try {
            ServicesConfigCache.getInstance().readServerDistribution("Nowhere.json");
        } catch (HttpStatusException e) {
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR_500, e.getHttpStatus());
        }
    }


}
