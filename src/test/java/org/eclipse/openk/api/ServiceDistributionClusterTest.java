/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class ServiceDistributionClusterTest {

    private ServiceDistributionCluster.ServiceDistribution createDistribution(
            boolean active, String name, String host, String urlPath, String healthUrlPath, String protocol,
            Integer portApp, Integer portHealth, String description) {

        ServiceDistributionCluster.ServiceDistribution sd = new ServiceDistributionCluster.ServiceDistribution();
        sd.setActive(true);
        sd.setName(name);
        sd.setHost(host);
        sd.setUrlPath(urlPath);
        if( healthUrlPath != null) {
            sd.setHealthUrlPath(healthUrlPath);
        }
        sd.setProtocol(protocol);
        sd.setPortApp(portApp);
        sd.setPortHealth(portHealth);
        sd.setDescription(description);

        return sd;
    }

    @Test
    public void testPojo() throws IOException {
        List<ServiceDistributionCluster.ServiceDistribution> distList = new ArrayList<>();
        distList.add(createDistribution(true, "Hugo", "1.2.3.4", "19/eleven",
                "healthy","httpx",
                1000, 2000, "Hugo ist toll"));
        distList.add(createDistribution(false, "Bruno Haferkamp", "2.3.4.5", "seven/of/nine", "httpxy",
                null,10, null, "Bruno ist noch toller"));
        ServiceDistributionCluster.ServiceDistribution[] arr
                = new ServiceDistributionCluster.ServiceDistribution[distList.size()];
        ServiceDistributionCluster cluster = new ServiceDistributionCluster();

        cluster.setDistributions(distList.toArray(arr));
        cluster.setClustername("Kleister");
        cluster.setActive(true);
        cluster.setDescription("The cluster of kleister!");

        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(cluster);

        ServiceDistributionCluster inCluster = om.readValue(json, ServiceDistributionCluster.class);

        assertEquals(cluster.getClustername(), inCluster.getClustername());
        assertEquals(cluster.isActive(), inCluster.isActive());
        assertEquals(cluster.getDescription(), inCluster.getDescription());
        for (int i = 0; i < cluster.getDistributions().length; i++) {
            assertEquals(cluster.getDistributions()[i].getName(), inCluster.getDistributions()[i].getName());
            assertEquals(cluster.getDistributions()[i].getHost(), inCluster.getDistributions()[i].getHost());
            assertEquals(cluster.getDistributions()[i].getUrlPath(), inCluster.getDistributions()[i].getUrlPath());
            assertEquals(cluster.getDistributions()[i].isActive(), inCluster.getDistributions()[i].isActive());
            assertEquals(cluster.getDistributions()[i].getPortApp(), inCluster.getDistributions()[i].getPortApp());
            assertEquals(cluster.getDistributions()[i].getPortHealth(), inCluster.getDistributions()[i].getPortHealth());
            assertEquals(cluster.getDistributions()[i].getDescription(), inCluster.getDistributions()[i].getDescription());
            assertEquals(cluster.getDistributions()[i].getProtocol(), inCluster.getDistributions()[i].getProtocol());
        }
    }
}
