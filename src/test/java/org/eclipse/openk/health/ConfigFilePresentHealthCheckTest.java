/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 * 
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.health;

import com.codahale.metrics.health.HealthCheck;
import org.eclipse.openk.api.ServiceDistributionCluster;
import org.eclipse.openk.core.controller.ServicesConfigCache;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConfigFilePresentHealthCheckTest {
    private boolean isHealthy(ConfigFilePresentHealthCheck hc) throws Exception {
        HealthCheck.Result r = Whitebox.invokeMethod(hc, "check");
        return r.isHealthy();
    }

    @Test
    public void testConfigFilePresent_False() throws Exception {
        ServicesConfigCache scc = ServicesConfigCache.getInstance();
        ServiceDistributionCluster[] oldCache = scc.getCache();

        try {
            ConfigFilePresentHealthCheck hc = new ConfigFilePresentHealthCheck();

            // test with invalid cluster
            Whitebox.setInternalState(scc, "cache", (Object) null);
            assertFalse(isHealthy(hc));

            // test with invalid cluster
            Whitebox.setInternalState(scc, "cache", new ServiceDistributionCluster[0]);
            assertFalse(isHealthy(hc));

            ServiceDistributionCluster[] arr = new ServiceDistributionCluster[1];
            arr[0] = new ServiceDistributionCluster();
            Whitebox.setInternalState(scc, "cache", arr);
            assertTrue(isHealthy(hc));
        } finally {
            Whitebox.setInternalState(scc, "cache", oldCache);
        }
    }
}
